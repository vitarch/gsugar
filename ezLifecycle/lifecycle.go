package ezLifecycle

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"golang.org/x/sync/errgroup"
)

// Lifecycle is component lifecycle.
type Lifecycle interface {
	Start(context.Context) error
	Stop(context.Context) error
}

// Instance is a pair of start and stop callbacks.
type Instance struct {
	OnStart func(context.Context) error
	OnStop  func(context.Context) error
}

// Option is an application option.
type Option func(o *options)

// options is an application options.
type options struct {
	id       string
	name     string
	version  string
	metadata map[string]string

	startTimeout time.Duration
	stopTimeout  time.Duration

	ctx   context.Context
	sigs  []os.Signal
	sigFn func(*App, os.Signal)
}

// StartTimeout with start timeout.
func StartTimeout(d time.Duration) Option {
	return func(o *options) { o.startTimeout = d }
}

// StopTimeout with stop timeout.
func StopTimeout(d time.Duration) Option {
	return func(o *options) { o.stopTimeout = d }
}

// Signal with os signals.
func Signal(fn func(*App, os.Signal), sigs ...os.Signal) Option {
	return func(o *options) {
		o.sigFn = fn
		o.sigs = sigs
	}
}

// App is an application components lifecycle manager
type App struct {
	opts      options
	instances []Instance

	cancel func()

	ctx context.Context
	mu  sync.Mutex
}

// New create an application lifecycle manager.
func New(opts ...Option) *App {
	lcOptions := options{
		startTimeout: time.Second * 30,
		stopTimeout:  time.Second * 30,
		sigs: []os.Signal{
			syscall.SIGTERM,
			syscall.SIGQUIT,
			syscall.SIGINT,
		},
		sigFn: func(a *App, sig os.Signal) {
			switch sig {
			case syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM:
				a.Stop()
			default:
			}
		},
	}
	for _, o := range opts {
		o(&lcOptions)
	}
	return &App{opts: lcOptions}
}

// Append register interface that are executed on application start and stop.
func (a *App) Append(lc Lifecycle) {
	a.instances = append(a.instances, Instance{
		OnStart: func(ctx context.Context) error {
			return lc.Start(ctx)
		},
		OnStop: func(ctx context.Context) error {
			return lc.Stop(ctx)
		},
	})
}

// AppendInstance register callbacks that are executed on application start and stop.
func (a *App) AppendInstance(ins Instance) {
	a.instances = append(a.instances, ins)
}

// Run executes all OnStart Instances registered with the application's Lifecycle.
func (a *App) Run() error {
	var ctx context.Context
	ctx, a.cancel = context.WithCancel(context.Background())
	g, ctx := errgroup.WithContext(ctx)
	for _, instance := range a.instances {
		instanceThis := instance
		if instanceThis.OnStop != nil {
			g.Go(func() error {
				<-ctx.Done() // wait for stop signal
				stopCtx, cancel := context.WithTimeout(context.Background(), a.opts.stopTimeout)
				defer cancel()
				return instanceThis.OnStop(stopCtx)
			})
		}
		if instanceThis.OnStart != nil {
			g.Go(func() error {
				startCtx, cancel := context.WithTimeout(context.Background(), a.opts.startTimeout)
				defer cancel()
				return instanceThis.OnStart(startCtx)
			})
		}
	}
	if len(a.opts.sigs) == 0 {
		return g.Wait()
	}
	c := make(chan os.Signal, len(a.opts.sigs))
	signal.Notify(c, a.opts.sigs...)
	g.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case sig := <-c:
				if a.opts.sigFn != nil {
					a.opts.sigFn(a, sig)
				}
			}
		}
	})
	return g.Wait()
}

// Stop gracefully stops the application.
func (a *App) Stop() {
	if a.cancel != nil {
		a.cancel()
	}
}
