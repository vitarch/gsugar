module gitee.com/vitarch/gsugar

go 1.19

require (
	github.com/go-playground/validator/v10 v10.14.1
	github.com/golang/snappy v0.0.4
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.29.1
	github.com/youpy/go-base57 v0.0.0-20210415141408-77e63e08858f
	golang.org/x/crypto v0.10.0
	golang.org/x/net v0.11.0
	golang.org/x/sync v0.3.0
	golang.org/x/time v0.3.0
)

require (
	github.com/gabriel-vasile/mimetype v1.4.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/lithammer/shortuuid/v3 v3.0.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.9.0 // indirect
	golang.org/x/text v0.10.0 // indirect
)
