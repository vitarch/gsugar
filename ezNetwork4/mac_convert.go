package ezNetwork4

import "strings"

func MacSplit(mac string, deli string) string {
	if len(mac) != 12 {
		return ""
	}
	macArr := make([]string, 0)
	macArr = append(macArr, mac[:2])
	macArr = append(macArr, mac[2:4])
	macArr = append(macArr, mac[4:6])
	macArr = append(macArr, mac[6:8])
	macArr = append(macArr, mac[8:10])
	macArr = append(macArr, mac[10:])
	return strings.Join(macArr, deli)
}
