package ezNetwork4

import (
	"fmt"
	"gitee.com/vitarch/gsugar/ezConvert"
	"net"
	"strconv"
	"strings"
)

func IsIPv4(ip string) bool {
	n := net.ParseIP(ip)
	if n == nil {
		return false
	} else {
		if n.To4() != nil {
			return true
		} else {
			return false
		}
	}
}

func IpToInt(ip string) (ipInt uint64) {
	ipSegArr := strings.Split(ip, ".")

	if len(ipSegArr) != 4 {
		return 0
	}

	segIP1, _ := strconv.ParseUint(ipSegArr[0], 10, 64)
	segIP2, _ := strconv.ParseUint(ipSegArr[1], 10, 64)
	segIP3, _ := strconv.ParseUint(ipSegArr[2], 10, 64)
	segIP4, _ := strconv.ParseUint(ipSegArr[3], 10, 64)

	ipInt = uint64(0)
	ipInt = ipInt + (segIP1 << 24)
	ipInt = ipInt + (segIP2 << 16)
	ipInt = ipInt + (segIP3 << 8)
	ipInt = ipInt + segIP4

	return ipInt
}

func IpIntToStr(ipInt uint64) (ip string) {
	return fmt.Sprintf("%s.%s.%s.%s",
		fmt.Sprintf("%d", byte((ipInt&0xFF000000)>>24)),
		fmt.Sprintf("%d", byte((ipInt&0x00FF0000)>>16)),
		fmt.Sprintf("%d", byte((ipInt&0x0000FF00)>>8)),
		fmt.Sprintf("%d", byte(ipInt&0x000000FF)))
}

func ReverseIPv4(ip string) string {
	if !IsIPv4(ip) {
		return ""
	}
	var result []string
	ipSegArr := strings.Split(ip, ".")
	for i := len(ipSegArr); i > 0; i-- {
		result = append(result, ipSegArr[i-1])
	}
	return strings.Join(result, ".")
}

func ReverseIPv4Int(ipInt int64) string {
	ipStr := IpIntToStr(uint64(ipInt))
	if !IsIPv4(ipStr) {
		return ""
	}
	var result []string
	ipSegArr := strings.Split(ipStr, ".")
	for i := len(ipSegArr); i > 0; i-- {
		result = append(result, ipSegArr[i-1])
	}
	return strings.Join(result, ".")
}

func IsIPv4Private(ip string) bool {
	for _, v := range IPv4Private {
		privateNetSplit := strings.Split(v, "/")
		calcNet := IPv4CalcNetwork(ip, ezConvert.ToInt64(privateNetSplit[1]))
		if calcNet == privateNetSplit[0] {
			return true
		}
	}
	return false
}

func IPv4CalcNetwork(ip string, netmaskNumeric int64) string {
	ipSegArr := strings.Split(ip, ".")
	if len(ipSegArr) != 4 {
		return ""
	}
	if netmaskNumeric < 0 || netmaskNumeric > 32 {
		return ""
	}

	maskInt := ^uint32(0) << uint(32-netmaskNumeric)
	maskSeg1 := uint8(maskInt >> 24)
	maskSeg2 := uint8(maskInt >> 16)
	maskSeg3 := uint8(maskInt >> 8)
	maskSeg4 := uint8(maskInt & uint32(255))

	ipSeg1, _ := strconv.Atoi(ipSegArr[0])
	ipSeg2, _ := strconv.Atoi(ipSegArr[1])
	ipSeg3, _ := strconv.Atoi(ipSegArr[2])
	ipSeg4, _ := strconv.Atoi(ipSegArr[3])

	networkSeg1 := maskSeg1 & uint8(ipSeg1)
	networkSeg2 := maskSeg2 & uint8(ipSeg2)
	networkSeg3 := maskSeg3 & uint8(ipSeg3)
	networkSeg4 := maskSeg4 & uint8(ipSeg4)

	return fmt.Sprintf("%d.%d.%d.%d", networkSeg1, networkSeg2, networkSeg3, networkSeg4)
}

func CalcCidrHostCount(maskint int) uint {
	hostCount := uint(0)
	hostBit := 32 - maskint - 1
	for ; hostBit >= 1; hostBit-- {
		hostCount += 1 << hostBit
	}

	return hostCount
}
