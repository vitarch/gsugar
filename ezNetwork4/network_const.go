package ezNetwork4

var (
	IPv4Private = []string{
		"10.0.0.0/8",
		"172.16.0.0/12",
		"192.168.0.0/16",
	}
)

const (
	DelimiterDash   = "-"
	DelimiterColon  = ":"
	DelimiterSubnet = "/"
)
