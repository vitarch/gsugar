package ezRequest

// Request 请求结构
type Request struct {
	Uri           string // The URL we just crawled
	UserAgent     string //指定请求的UserAgent信息
	ContentType   string // Our content-type
	ContentLength int64  //返回的数据长度
	Body          []byte // The actual page content.
	Headers       map[string]string
	RequestId     string
	Host          string
}

func NewRequest() *Request {
	return &Request{
		Uri:           "",
		UserAgent:     "",
		ContentType:   "",
		ContentLength: 0,
		Body:          nil,
		Headers:       make(map[string]string),
		RequestId:     "",
		Host:          "",
	}
}

func (s *Request) SetContentType(value string) {
	s.ContentType = value
	s.Headers[HeaderKeyOfRequestContentType] = value
}

func (s *Request) SetUserAgent(value string) {
	s.UserAgent = value
	s.Headers[HeaderKeyOfRequestUserAgent] = value
}

func (s *Request) SetCookies(cookies string) {
	s.Headers[HeaderKeyOfRequestCookies] = cookies
}

func (s *Request) SetReferer(refer string) {
	s.Headers[HeaderKeyOfRequestReferer] = refer
}

func (s *Request) SetRequestId(id string) {
	s.RequestId = id
	s.Headers[DefaultXreqKeyword] = id
}

func (s *Request) SetHost(host string) {
	s.Headers[HeaderKeyOfRequestHost] = host
}

func (s *Request) SetHeader(k, v string) {
	s.Headers[k] = v
}

type Response struct {
	StatusCode int                 // HTTP code,错误时为-1
	Result     []byte              //请求结果
	Error      error               // The Error Message
	Headers    map[string][]string //HTTP请求响应的Header
	RemoteAddr string
}

func NewResponse() *Response {
	return &Response{
		StatusCode: 0,
		Result:     nil,
		Error:      nil,
		Headers:    make(map[string][]string),
		RemoteAddr: "",
	}
}
