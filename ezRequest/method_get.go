package ezRequest

import (
	"io"
	"net/http"
)

func GET(w *Request, r *Response) {

	var resp *http.Response

	client := &http.Client{}

	req, errRequestInit := http.NewRequest(http.MethodGet, w.Uri, nil)
	if errRequestInit != nil {
		r.Error = errRequestInit
		return
	}
	for k, v := range w.Headers {
		req.Header.Set(k, v)
	}

	if len(w.RequestId) > 0 {
		req.Header.Set(DefaultXreqKeyword, w.RequestId)
	}

	if len(w.Host) > 0 {
		req.Host = w.Host
	}

	rr, errHttpRequest := client.Do(req)
	if errHttpRequest != nil {
		r.Error = errHttpRequest
		return
	}

	resp = rr

	r.StatusCode = resp.StatusCode

	result, errBodyRead := io.ReadAll(resp.Body)
	resp.Body.Close()
	if errBodyRead != nil {
		r.Error = errBodyRead
		return
	}

	r.Headers = make(map[string][]string)
	for k, _ := range resp.Header {
		r.Headers[k] = resp.Header.Values(k)
	}

	r.Result = result
	return
}

func DoGet(w *Request, r *Response, client *http.Client) {

	var resp *http.Response

	req, errRequestInit := http.NewRequest(http.MethodGet, w.Uri, nil)
	if errRequestInit != nil {
		r.Error = errRequestInit
		return
	}
	for k, v := range w.Headers {
		req.Header.Set(k, v)
	}

	if len(w.RequestId) > 0 {
		req.Header.Set(DefaultXreqKeyword, w.RequestId)
	}

	if len(w.Host) > 0 {
		req.Host = w.Host
	}

	rr, errHttpRequest := client.Do(req)
	if errHttpRequest != nil {
		r.Error = errHttpRequest
		return
	}

	resp = rr

	r.StatusCode = resp.StatusCode

	result, errBodyRead := io.ReadAll(resp.Body)
	resp.Body.Close()
	if errBodyRead != nil {
		r.Error = errBodyRead
		return
	}

	r.Headers = make(map[string][]string)
	for k, _ := range resp.Header {
		r.Headers[k] = resp.Header.Values(k)
	}

	r.Result = result
	return
}
