package ezRequest

import (
	"net/url"
	"strconv"
	"strings"
)

type Url struct {
	Scheme   string
	Username string
	Password string
	Host     string
	Port     int
	Path     string
	Fragment string
	RawQuery map[string][]string
}

func ParseURL(src string) (Url, error) {
	var result Url
	urlParse, err := url.Parse(src)
	if err != nil {
		return result, err
	}
	result.Scheme = urlParse.Scheme
	result.Username = urlParse.User.Username()
	result.Password, _ = urlParse.User.Password()
	hostSplit := strings.Split(urlParse.Host, ":")
	if len(hostSplit) == 1 {
		result.Host = hostSplit[0]
	} else {
		result.Host = hostSplit[0]
		result.Port, _ = strconv.Atoi(hostSplit[1])
		if result.Port == 0 {
			if strings.ToLower(result.Scheme) == "http" {
				result.Port = 80
			}
			if strings.ToLower(result.Scheme) == "https" {
				result.Port = 443
			}
		}
	}

	result.Path = urlParse.Path
	result.Fragment = urlParse.Fragment
	rQuery, err := url.ParseQuery(urlParse.RawQuery)
	if err != nil {
		return result, err
	}
	result.RawQuery = make(map[string][]string)
	for k, v := range rQuery {
		result.RawQuery[k] = v
	}
	return result, nil
}

func BuildUrl(baseURL string, params map[string]string) string {
	uu, _ := url.Parse(baseURL)
	q := uu.Query()
	//构建查询参数
	for k, v := range params {
		q.Set(k, v)
	}
	uu.RawQuery = q.Encode()
	return uu.String()
}
