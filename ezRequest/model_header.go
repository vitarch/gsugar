package ezRequest

const (
	HeaderKeyOfResponseCookies = "Set-Cookie"

	HeaderKeyOfRequestHost        = "Host"
	HeaderKeyOfRequestCookies     = "Cookie"
	HeaderKeyOfRequestReferer     = "Referer"
	HeaderKeyOfRequestContentType = "Content-Type"
	HeaderKeyOfRequestUserAgent   = "UserAgent"
	HeaderKeyOfRequestAcceptLang  = "Accept-Language"
	DefaultXffKeyword             = "X-Forwarded-For"
	DefaultXfhKeyword             = "X-Forwarded-Host"
	DefaultXfpKeyword             = "X-Forwarded-Proto"
	DefaultXreqKeyword            = "X-Request-ID"
)

const (
	AccessControlAllowHeaders     = "Access-Control-Allow-Headers"
	AccessControlAllowMethods     = "Access-Control-Allow-Methods"
	AccessControlAllowOrigin      = "Access-Control-Allow-Origin"
	AccessControlAllowCredentials = "Access-Control-Allow-Credentials"
	AccessControlMaxAge           = "Access-Control-Max-Age"
	AccessControlExposeHeaders    = "Access-Control-Expose-Headers"
)
