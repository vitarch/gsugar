package ezRequest

import (
	"fmt"
	"io"
	"net/http"
)

func WGetBytes(uri string) ([]byte, error) {

	req, err := http.Get(uri)
	if err != nil {
		return nil, err
	}

	if req.StatusCode != HTTPStatusOK {
		return nil, fmt.Errorf("http status error:%d", req.StatusCode)
	}

	result, err := io.ReadAll(req.Body)

	if err != nil {
		return nil, err
	}

	defer req.Body.Close()

	return result, nil
}
