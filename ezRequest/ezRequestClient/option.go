package ezRequestClient

import "net/http"

type ClientOption func(*clientOptions)

type clientOptions struct {
	transport *http.Transport
}

func WithTransport(s *http.Transport) ClientOption {
	return func(o *clientOptions) {
		o.transport = s
	}
}
