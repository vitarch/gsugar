package ezRequestClient

import "net/http"

func New(opts ...ClientOption) *http.Client {
	options := clientOptions{transport: nil}
	for _, opt := range opts {
		opt(&options)
	}

	c := &http.Client{}
	if options.transport != nil {
		c.Transport = options.transport
	}

	return c
}

func Default() *http.Client {
	return &http.Client{}
}
