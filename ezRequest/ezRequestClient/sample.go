package ezRequestClient

import (
	"context"
	"net"
	"net/http"
	"time"
)

func CustomTransport(dialer *net.Dialer) *http.Transport {
	return &http.Transport{
		DialContext: func(ctx context.Context, network, address string) (net.Conn, error) {
			return dialer.DialContext(ctx, network, address)
		},
	}
}

func CustomDialer(proto, resolveSrv string, timeout time.Duration) *net.Dialer {
	return &net.Dialer{
		Resolver: &net.Resolver{
			PreferGo: true,
			Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
				d := net.Dialer{
					Timeout: timeout,
				}
				return d.DialContext(ctx, proto, resolveSrv)
			},
		},
	}
}
