package ezRequest

const (
	ContentTypeJSON        = "application/json"
	ContentTypeXLSX        = "application/xlsx"
	ContentTypeOctetStream = "application/octet-stream"
	ContentTypeUrlencoded  = "application/x-www-form-urlencoded"
	ContentTypeFormData    = "multipart/form-data"
	ContentTypeXml         = "application/xml"
	ContentTypeBinary      = "application/octet-stream"
)
