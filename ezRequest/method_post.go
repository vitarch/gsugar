package ezRequest

import (
	"bytes"
	"context"
	"io"
	"net/http"
)

func POST(w *Request, r *Response) {
	buffer := bytes.NewBuffer(w.Body)

	req, errRequestInit := http.NewRequest(http.MethodPost, w.Uri, buffer)
	if errRequestInit != nil {
		r.StatusCode = -1
		r.Error = errRequestInit
		return
	}

	for k, v := range w.Headers {
		req.Header.Set(k, v)
	}

	if len(w.RequestId) > 0 {
		req.Header.Set(DefaultXreqKeyword, w.RequestId)
	}

	if len(w.Host) > 0 {
		req.Host = w.Host
	}

	w.ContentLength = req.ContentLength

	client := &http.Client{}

	resp, errDoRequest := client.Do(req.WithContext(context.TODO()))
	if errDoRequest != nil {
		r.StatusCode = -1
		r.Error = errDoRequest
		return
	}

	defer resp.Body.Close()

	respBytes, errBodyRead := io.ReadAll(resp.Body)
	if errBodyRead != nil {
		r.StatusCode = -1
		r.Error = errBodyRead
		return
	}

	r.Headers = make(map[string][]string)
	for k, _ := range resp.Header {
		r.Headers[k] = resp.Header.Values(k)
	}

	r.StatusCode = resp.StatusCode
	r.Result = respBytes
	return
}

func DoPost(w *Request, r *Response, client *http.Client) {
	buffer := bytes.NewBuffer(w.Body)

	req, errRequestInit := http.NewRequest(http.MethodPost, w.Uri, buffer)
	if errRequestInit != nil {
		r.StatusCode = -1
		r.Error = errRequestInit
		return
	}

	for k, v := range w.Headers {
		req.Header.Set(k, v)
	}

	if len(w.RequestId) > 0 {
		req.Header.Set(DefaultXreqKeyword, w.RequestId)
	}

	if len(w.Host) > 0 {
		req.Host = w.Host
	}

	w.ContentLength = req.ContentLength

	resp, errDoRequest := client.Do(req.WithContext(context.TODO()))
	if errDoRequest != nil {
		r.StatusCode = -1
		r.Error = errDoRequest
		return
	}

	defer resp.Body.Close()

	respBytes, errBodyRead := io.ReadAll(resp.Body)
	if errBodyRead != nil {
		r.StatusCode = -1
		r.Error = errBodyRead
		return
	}

	r.Headers = make(map[string][]string)
	for k, _ := range resp.Header {
		r.Headers[k] = resp.Header.Values(k)
	}

	r.StatusCode = resp.StatusCode
	r.Result = respBytes
	return
}
