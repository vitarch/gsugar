package ezRequest

import "net/http"

func StatusErrorMsg(errcode int) string {
	switch errcode {
	case 404:
		return "页面不存在"
	case 403:
		return "未授权"
	case 500:
		return "服务暂时不可用"
	case 503:
		return "服务暂时不可用"
	default:
		return "内部服务器出错"

	}
}

const (
	HTTPStatusOK = http.StatusOK
)
