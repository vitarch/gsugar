package ezConvert

import (
	"fmt"
	"strconv"
	"strings"
)

// ToString 转字符
func ToString(src interface{}) string {
	switch val := src.(type) {
	case nil:
		return ""
	case int:
		return strconv.Itoa(val)
	case int32:
		return fmt.Sprintf("%d", val)
	case int64:
		return fmt.Sprintf("%d", val)
	case float32:
		return fmt.Sprintf("%f", val)
	case float64:
		return fmt.Sprintf("%f", val)
	case string:
		return val
	case bool:
		return fmt.Sprintf("%t", val)
	default:
		return fmt.Sprintf("%v", val)
	}
}

func SliceToString(src []interface{}, sep string) string {
	result := make([]string, 0)
	for _, v := range src {
		result = append(result, ToString(v))
	}
	return strings.Join(result, sep)
}

func IntSliceToString(src []int, sep string) string {
	result := make([]string, 0)
	for _, v := range src {
		result = append(result, ToString(v))
	}
	return strings.Join(result, sep)
}

func MacSplitWithDash(src string) string {
	if len(src) != 12 {
		return ""
	}
	macBytes := []byte(src)
	result := make([]string, 0)
	for i := 0; i < 12; i++ {
		if i > 0 && i%2 == 0 {
			result = append(result, "-")
		}
		result = append(result, string(macBytes[i]))
	}
	return strings.Join(result, "")
}

func MacSplitWithColon(src string) string {
	if len(src) != 12 {
		return ""
	}
	macBytes := []byte(src)
	result := make([]string, 0)
	for i := 0; i < 12; i++ {
		if i > 0 && i%2 == 0 {
			result = append(result, ":")
		}
		result = append(result, string(macBytes[i]))
	}
	return strings.Join(result, "")
}
