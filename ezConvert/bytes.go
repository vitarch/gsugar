package ezConvert

import (
	"bytes"
	"encoding/binary"
)

func BytesToInt64(bys []byte) int64 {
	byteBuff := bytes.NewBuffer(bys)
	var data int64
	binary.Read(byteBuff, binary.BigEndian, &data)
	return data
}

func BytesToInt32(bys []byte, length int) int32 {
	byteBuff := bytes.NewBuffer(bys)
	var data int32
	binary.Read(byteBuff, binary.BigEndian, &data)
	return data
}

func BytesToInt(bys []byte) int {
	byteBuff := bytes.NewBuffer(bys)
	var data int64
	binary.Read(byteBuff, binary.BigEndian, &data)
	return int(data)
}
