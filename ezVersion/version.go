package ezVersion

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	IsEq int = iota
	IsNewer
	IsOlder
)

func NewVersion(v string, b int, c uint8) *Version {
	return &Version{Version: v, Build: b, Feature: c}
}

type Version struct {
	//主版本号
	Version string `json:"version"`

	//内部版本号
	Build int `json:"build"`

	//版本标识码
	Feature uint8 `json:"feature"`
}

func (v *Version) Full() string {
	if v.Build > 0 {
		return fmt.Sprintf("%s (Build %d)", v.Version, v.Build)
	} else {
		return v.MainVersion()
	}
}

func (v *Version) MainVersion() string {
	return v.Version
}

func (v *Version) BuildVersion() string {
	return fmt.Sprintf("%d", v.Build)
}

func (v *Version) FeatureVersion() uint8 {
	return v.Feature
}

func (v *Version) CompatMain(b string) (result int) {
	if v.Version == b {
		return IsEq
	}
	if mainLessThan(v.Version, b) {
		return IsOlder
	}
	return IsNewer
}

func (v *Version) CompatAll(b string) (result int) {
	if v.Version == b {
		return IsEq
	}
	if lessThan(v.Version, b) {
		return IsOlder
	}
	return IsNewer
}

func getSubVersionWithMain(v string, position int) int64 {
	arr := strings.Split(v, ".")
	if len(arr) < 3 {
		return 0
	}
	res, _ := strconv.ParseInt(arr[position], 10, 64)
	return res
}

func getSubVersionWithAll(v string, position int) int64 {
	arr := strings.Split(v, ".")
	if len(arr) < 4 {
		return 0
	}
	res, _ := strconv.ParseInt(arr[position], 10, 64)
	return res
}

func proto(v string) int64 {
	return getSubVersionWithMain(v, 0)
}

func major(v string) int64 {
	return getSubVersionWithMain(v, 1)
}

func minor(v string) int64 {
	return getSubVersionWithMain(v, 2)
}

func Build(v string) int64 {
	return getSubVersionWithAll(v, 3)
}

func mainLessThan(a string, b string) bool {
	va := proto(a)
	vb := proto(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	va = major(a)
	vb = major(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	va = minor(a)
	vb = minor(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	return false
}

func lessThan(a string, b string) bool {
	va := proto(a)
	vb := proto(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	va = major(a)
	vb = major(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	va = minor(a)
	vb = minor(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	va = Build(a)
	vb = Build(b)
	if va > vb {
		return false
	} else if va < vb {
		return true
	}

	return false
}
