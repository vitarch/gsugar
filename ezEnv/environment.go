package ezEnv

import (
	"flag"
	"os"
	"strings"
)

const (
	EnvDev  = "dev"  //开发环境
	EnvTest = "test" //测试环境
	EnvPre  = "pre"  //预发布环境
	EnvProd = "prod" //生产环境
)

// 默认值
const (
	_countryDefault = "cn"
	_regionDefault  = "region"
	_zoneDefault    = "zone"
	_workEnvDefault = EnvDev
)

var (
	Country  string //国家
	Region   string //大区
	Zone     string //可用区
	Hostname string //操作系统主机名
	AppID    string //环境应用标识
	WorkEnv  string //当前运行环境类型
	Color    string //染色标识
)

// 隐式初始化
func init() {
	var err error
	Hostname = os.Getenv("HOSTNAME")
	if strings.ReplaceAll(Hostname, " ", "") == "" {
		Hostname, err = os.Hostname()
		if err != nil {
			Hostname = "localhost"
		}
	}
	addFlag(flag.CommandLine)
}

func addFlag(fs *flag.FlagSet) {
	fs.StringVar(&Country, "country", readEnv("COUNTRY", _countryDefault), "country")
	fs.StringVar(&Region, "region", readEnv("REGION", _regionDefault), "region")
	fs.StringVar(&Zone, "zone", readEnv("ZONE", _zoneDefault), "zone")
	fs.StringVar(&AppID, "appid", readEnv("APPID", ""), "app id")
	fs.StringVar(&WorkEnv, "work.env", readEnv("WORK_ENV", _workEnvDefault), "work env")
	fs.StringVar(&Color, "work.color", readEnv("WORK_COLOR", ""), "bu color")
}

func readEnv(envKey, defaultValue string) string {
	v := os.Getenv(envKey)
	if strings.TrimSpace(v) == "" {
		return defaultValue
	}
	return v
}
