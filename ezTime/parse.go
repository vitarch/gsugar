package ezTime

import (
	"time"
)

func ParseUTC(utc string, format string) (local time.Time, err error) {
	timeUTC, err := time.Parse(format, utc)
	if err != nil {
		return local, err
	}
	return timeUTC.Local(), nil
}

func ParseShort(s string) (tDst time.Time) {
	tDst, _ = time.ParseInLocation(FormatDay, s, time.Local)
	return
}

func ParseLong(s string) (tDst time.Time) {
	tDst, _ = time.ParseInLocation(FormatDatetime, s, time.Local)
	return
}
