package ezTime

func WeekdayEn2Cn(s string) string {
	switch s {
	case "Monday":
		return "星期一"
	case "Tuesday":
		return "星期二"
	case "Wednesday":
		return "星期三"
	case "Thursday":
		return "星期四"
	case "Friday":
		return "星期五"
	case "Saturday":
		return "星期六"
	case "Sunday":
		return "星期天"
	default:
		return ""
	}
}
