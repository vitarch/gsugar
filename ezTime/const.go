package ezTime

const (
	FormatDatetime        = "2006-01-02 15:04:05"
	FormatDatetimeCompact = "20060102150405"
	FormatDay             = "2006-01-02"
	FormatDateCompact     = "20060102"
	FormatTime            = "15:04:05"

	FormatUTC  = "2006-01-02T15:04:05Z"
	FormatUTC1 = "2006-01-02T15:04:05"
	FormatUTC2 = "2006-01-02T15:04Z"

	FormatISO8601 = "2006-01-02T15:04:05.999-07:00"
)
