package ezTime

import (
	"math/rand"
	"time"
)

func SleepHour() {
	time.Sleep(time.Hour)
}

func SleepSecond(sleep int) {
	time.Sleep(time.Duration(sleep) * time.Second)
}

func SleepRandSecond(max int, seed int64) {
	rand.Seed(seed)
	sleep := rand.Intn(max)
	time.Sleep(time.Duration(sleep) * time.Second)
}

func SleepRandMilli(max int, seed int64) {
	rand.Seed(seed)
	sleep := rand.Intn(max)
	time.Sleep(time.Duration(sleep) * time.Millisecond)
}
