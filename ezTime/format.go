package ezTime

import (
	"fmt"
	"gitee.com/vitarch/gsugar/ezConvert"
	"time"
)

func FormatUnix(unixTime interface{}, format string) string {
	var convertTime int64
	switch unixTime.(type) {
	case string:
		convertTime = ezConvert.ToInt64(unixTime.(string))
	case int:
		convertTime = int64(unixTime.(int))
	case int8:
		convertTime = int64(unixTime.(int8))
	case int16:
		convertTime = int64(unixTime.(int16))
	case int32:
		convertTime = int64(unixTime.(int32))
	}
	return time.Unix(convertTime, 0).Format(format)
}

func FormatDelay(ts time.Time) string {
	return fmt.Sprintf("%v", time.Since(ts))
}
