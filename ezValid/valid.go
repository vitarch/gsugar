package ezValid

import (
	"errors"
	validator "github.com/go-playground/validator/v10"
	"regexp"
)

var validate *validator.Validate

func IsCronTab(src string) bool {
	regular := `^[ a-zA-Z0-9/,\*]+$`
	return regexp.MustCompile(regular).MatchString(src)
}

func IsURL(src string) bool {
	regular := `^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=_]*)?`
	return regexp.MustCompile(regular).MatchString(src)
}

func IsAlphaDash(str string) bool {
	reg := regexp.MustCompile(`[^\d\w-_]`)
	return !reg.MatchString(str)
}

func IsEmail(src string) bool {
	var regular = `^[\w!#$%&'*+/=?^_` + "`" + `{|}~-]+(?:\.[\w!#$%&'*+/=?^_` + "`" + `{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[a-zA-Z0-9](?:[\w-]*[\w])?$`
	return regexp.MustCompile(regular).MatchString(src)
}

func IsEmptyString(src string) bool {
	return len(src) == 0
}

func IsNumeric(str string) bool {
	reg := regexp.MustCompile(`[^\d]`)
	return !reg.MatchString(str)
}

func IsAlphaNumeric(str string) bool {
	reg := regexp.MustCompile(`[^\d\w]`)
	return !reg.MatchString(str)
}

func StructValid(src interface{}) (bool, error) {
	validate = validator.New()
	err := validate.Struct(src)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return false, errors.New("valid error:" + err.Error())
		}
		return false, errors.New("not valid:" + err.Error())
	}
	return true, nil
}

type ValidErrorDetail struct {
	Namespace  string
	FieldName  string
	FieldTag   string
	FieldType  string
	FieldValue interface{}
}

func TagValid(src interface{}) (bool, []*ValidErrorDetail, error) {
	errDetails := make([]*ValidErrorDetail, 0)
	validate = validator.New()
	err := validate.Struct(src)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return false, errDetails, errors.New("valid error:" + err.Error())
		} else {
			for _, errDetail := range err.(validator.ValidationErrors) {
				var tmp ValidErrorDetail
				tmp.Namespace = errDetail.Namespace()
				tmp.FieldName = errDetail.Field()
				tmp.FieldTag = errDetail.Tag()
				tmp.FieldType = errDetail.Kind().String()
				tmp.FieldValue = errDetail.Value()
				errDetails = append(errDetails, &tmp)
			}
			return false, errDetails, errors.New("not valid:" + err.Error())
		}
	}
	return true, nil, nil
}

func IsMobile(src string) bool {
	regular := "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$"
	return regexp.MustCompile(regular).MatchString(src)
}

func IsCitizenNo(src string) bool {
	regular := "^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$"
	return regexp.MustCompile(regular).MatchString(src)
}
