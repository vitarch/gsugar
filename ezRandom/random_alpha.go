package ezRandom

import (
	"math/rand"
	"time"
)

const (
	KeyTableAlpha   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	KeyTableNumeric = "0123456789"
	KeyTableAll     = KeyTableNumeric + KeyTableAlpha
)

// RandDefault 生成随机字符串
func RandDefault(length int) string {
	if length == 0 {
		length = 6
	}
	bytes := []byte(KeyTableAll)
	bytesLen := len(bytes)
	s := make([]byte, 0)
	randNew := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < length; i++ {
		s = append(s, bytes[randNew.Intn(bytesLen)])
	}
	return string(s)
}

// RandAlpha 生成随机字符串
func RandAlpha(length int) string {
	if length == 0 {
		length = 6
	}
	bytes := []byte(KeyTableAlpha)
	bytesLen := len(bytes)
	s := make([]byte, 0)
	randNew := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < length; i++ {
		s = append(s, bytes[randNew.Intn(bytesLen)])
	}
	return string(s)
}

func RandNumeric(length int) string {
	bytes := []byte(KeyTableNumeric)
	bytesLen := len(bytes)
	s := make([]byte, 0)
	randNew := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < length; i++ {
		s = append(s, bytes[randNew.Intn(bytesLen)])
	}
	return string(s)
}
