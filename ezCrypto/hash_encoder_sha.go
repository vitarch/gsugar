package ezCrypto

import (
	"crypto/sha1"
	"encoding/hex"
)

func SHA1Default(s string) string {
	h := sha1.New()
	h.Write([]byte(s))
	rs := hex.EncodeToString(h.Sum(nil))
	return rs
}
