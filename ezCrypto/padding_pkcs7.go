package ezCrypto

import "bytes"

type PKCS7 struct{}

func (p *PKCS7) Pad(src []byte, blockSize int) []byte {
	padding := blockSize - len(src)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, padText...)
}

func (p *PKCS7) UnPad(src []byte) []byte {
	length := len(src)
	paddingNum := int(src[length-1])
	return src[:(length - paddingNum)]
}

type PKCS5 struct{}

func (p *PKCS5) Pad(src []byte, blockSize int) []byte {
	padding := blockSize - len(src)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, padText...)
}

func (p *PKCS5) UnPad(src []byte) []byte {
	length := len(src)
	paddingNum := int(src[length-1])
	return src[:(length - paddingNum)]
}

type PKCSZero struct{}

func (p *PKCSZero) Pad(src []byte, blockSize int) []byte {
	padding := blockSize - len(src)%blockSize
	padText := bytes.Repeat([]byte{byte(0)}, padding)
	return append(src, padText...)
}

func (p *PKCSZero) UnPad(src []byte) []byte {
	for i := len(src) - 1; ; i-- {
		if src[i] != 0 {
			return src[:i+1]
		}
	}
	return nil
}
