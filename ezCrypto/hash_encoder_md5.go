package ezCrypto

import (
	"crypto/md5"
)

func Md5(srcBytes []byte) []byte {
	h := md5.New()
	h.Write(srcBytes)
	return h.Sum(nil)
}

func Md5Hex(src string) string {
	return HashHex(Md5([]byte(src)))
}
