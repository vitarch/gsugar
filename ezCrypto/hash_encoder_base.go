package ezCrypto

import "encoding/hex"

func HashHex(hashBytes []byte) string {
	return hex.EncodeToString(hashBytes)
}
