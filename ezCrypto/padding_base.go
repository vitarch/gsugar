package ezCrypto

const PaddingModePkcs5 = "PKCS5"
const PaddingModePkcs7 = "PKCS7"

type Padding interface {
	Pad(src []byte, blockSize int) []byte
	UnPad(src []byte) []byte
}
