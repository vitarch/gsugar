package ezCrypto

import "encoding/base64"

func Base64DecodeS2S(src string) (string, error) {
	result, err := base64.StdEncoding.DecodeString(src)
	if err != nil {
		return "", err
	}
	return string(result), nil
}

func Base64DecodeB2S(src []byte) string {
	result, err := base64.StdEncoding.DecodeString(string(src))
	if err != nil {
		return ""
	}
	return string(result)
}

func Base64DecodeS2B(src string) (result []byte, err error) {
	result, err = base64.StdEncoding.DecodeString(src)
	return
}

func Base64EncodeS2S(src string) string {
	return base64.StdEncoding.EncodeToString([]byte(src))
}

func Base64EncodeB2S(src []byte) string {
	return base64.StdEncoding.EncodeToString(src)
}

func Base64EncodeS2B(src string) (result []byte) {
	result = []byte(base64.StdEncoding.EncodeToString([]byte(src)))
	return
}

func Base64EncodeB2B(src []byte) (result []byte) {
	result = []byte(base64.StdEncoding.EncodeToString(src))
	return
}

func Base64DecodeWithTable(src []byte, base64table string) (dst []byte, err error) {
	var coder = base64.NewEncoding(base64table)
	dst, err = coder.DecodeString(string(src))
	if err != nil {
		return
	}
	return dst, nil
}

func Base64EncodeWithTable(src []byte, base64table string) (dst []byte) {
	var coder = base64.NewEncoding(base64table)
	return []byte(coder.EncodeToString(src))
}
