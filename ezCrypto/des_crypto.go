package ezCrypto

import (
	"crypto/cipher"
	"crypto/des"
	"errors"
)

func DesEncrypt(plainData []byte, key []byte, vi []byte, padMode string) ([]byte, error) {
	block, _ := des.NewCipher(key)
	if block.BlockSize() != len(vi) {
		return nil, errors.New("vi length not match block size")
	}
	switch padMode {
	case PaddingModePkcs5:
		pkcs := new(PKCS5)
		plainData = pkcs.Pad(plainData, block.BlockSize())
	case PaddingModePkcs7:
		pkcs := new(PKCS7)
		plainData = pkcs.Pad(plainData, block.BlockSize())
	default:
		pkcs := new(PKCSZero)
		plainData = pkcs.Pad(plainData, block.BlockSize())
	}
	blockMode := cipher.NewCBCEncrypter(block, vi)
	cipherData := make([]byte, len(plainData))
	blockMode.CryptBlocks(cipherData, plainData)
	return cipherData, nil
}

func DesDecrypt(cipherData []byte, key []byte, vi []byte, padMode string) ([]byte, error) {
	block, _ := des.NewCipher(key)
	if block.BlockSize() != len(vi) {
		return nil, errors.New("vi length not match block size")
	}
	plainData := make([]byte, len(cipherData))
	blockMode := cipher.NewCBCDecrypter(block, vi)
	blockMode.CryptBlocks(plainData, cipherData)

	switch padMode {
	case PaddingModePkcs5:
		pkcs := new(PKCS5)
		plainData = pkcs.UnPad(plainData)
	case PaddingModePkcs7:
		pkcs := new(PKCS7)
		plainData = pkcs.UnPad(plainData)
	default:
		pkcs := new(PKCSZero)
		plainData = pkcs.UnPad(plainData)
	}
	return plainData, nil
}
