package ezConn

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha1"
	"errors"
	"io"

	"golang.org/x/crypto/pbkdf2"
)

func Encryption(rwc io.ReadWriteCloser, salt, key, iv []byte) (io.ReadWriteCloser, error) {
	w, err := NewWriter(rwc, salt, key, iv)
	if err != nil {
		return nil, err
	}
	return WrapReadWriteCloser(NewReader(rwc, salt, key, iv), w, func() error {
		return rwc.Close()
	}), nil
}

// NewWriter returns a new Writer that encrypts bytes to w.
func NewWriter(w io.Writer, salt, key, iv []byte) (*Writer, error) {
	key = pbkdf2.Key(key, salt, 64, aes.BlockSize, sha1.New)

	if len(iv) != aes.BlockSize {
		return nil, errors.New("iv length invalid")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	return &Writer{
		w: w,
		enc: &cipher.StreamWriter{
			S: cipher.NewCFBEncrypter(block, iv),
			W: w,
		},
		key: key,
		iv:  iv,
	}, nil
}

// Writer is an io.Writer that can write encrypted bytes.
// Now it only support aes-128-cfb.
type Writer struct {
	w      io.Writer
	enc    *cipher.StreamWriter
	key    []byte
	iv     []byte
	ivSend bool
	err    error
}

// Write satisfies the io.Writer interface.
func (w *Writer) Write(p []byte) (nRet int, errRet error) {
	if w.err != nil {
		return 0, w.err
	}

	// When write is first called, iv will be written to w.w
	if !w.ivSend {
		w.ivSend = true
		_, errRet = w.w.Write(w.iv)
		if errRet != nil {
			w.err = errRet
			return
		}
	}

	nRet, errRet = w.enc.Write(p)
	if errRet != nil {
		w.err = errRet
	}
	return
}

// NewReader returns a new Reader that decrypts bytes from r
func NewReader(r io.Reader, salt, key, iv []byte) *Reader {
	key = pbkdf2.Key(key, salt, 64, aes.BlockSize, sha1.New)

	return &Reader{
		r:   r,
		key: key,
		iv:  iv,
	}
}

// Reader is an io.Reader that can read encrypted bytes.
// Now it only supports aes-128-cfb.
type Reader struct {
	r   io.Reader
	dec *cipher.StreamReader
	key []byte
	iv  []byte
	err error
}

// Read satisfies the io.Reader interface.
func (r *Reader) Read(p []byte) (nRet int, errRet error) {
	if r.err != nil {
		return 0, r.err
	}

	if r.dec == nil {

		block, err := aes.NewCipher(r.key)
		if err != nil {
			errRet = err
			return
		}
		r.dec = &cipher.StreamReader{
			S: cipher.NewCFBDecrypter(block, r.iv),
			R: r.r,
		}
	}

	nRet, errRet = r.dec.Read(p)
	if errRet != nil {
		r.err = errRet
	}
	return
}
