package ezConn

import (
	"github.com/pkg/errors"
	"io"
	"net"
	"time"
)

type WrapReadWriteCloserConn struct {
	io.ReadWriteCloser
	hideConn net.Conn
}

func NewWrapReadWriteCloserConn(rwc io.ReadWriteCloser, hideConn net.Conn) net.Conn {
	return &WrapReadWriteCloserConn{
		ReadWriteCloser: rwc,
		hideConn:        hideConn,
	}
}

func (rwcc *WrapReadWriteCloserConn) LocalAddr() net.Addr {
	if rwcc.hideConn != nil {
		return rwcc.hideConn.LocalAddr()
	}
	return (*net.TCPAddr)(nil)
}

func (rwcc *WrapReadWriteCloserConn) RemoteAddr() net.Addr {
	if rwcc.hideConn != nil {
		return rwcc.hideConn.RemoteAddr()
	}
	return (*net.TCPAddr)(nil)
}

func (rwcc *WrapReadWriteCloserConn) SetDeadline(t time.Time) error {
	if rwcc.hideConn != nil {
		return rwcc.hideConn.SetDeadline(t)
	}
	return &net.OpError{Op: "set", Net: "wrap", Source: nil, Addr: nil, Err: errors.New("deadline not supported")}
}

func (rwcc *WrapReadWriteCloserConn) SetReadDeadline(t time.Time) error {
	if rwcc.hideConn != nil {
		return rwcc.hideConn.SetReadDeadline(t)
	}
	return &net.OpError{Op: "set", Net: "wrap", Source: nil, Addr: nil, Err: errors.New("deadline not supported")}
}

func (rwcc *WrapReadWriteCloserConn) SetWriteDeadline(t time.Time) error {
	if rwcc.hideConn != nil {
		return rwcc.hideConn.SetWriteDeadline(t)
	}
	return &net.OpError{Op: "set", Net: "wrap", Source: nil, Addr: nil, Err: errors.New("deadline not supported")}
}
