package ezConn

import (
	"io"
	"sync"
)

type ReadWriteCloser struct {
	r io.Reader
	w io.Writer

	closer func() error
	closed bool

	lock sync.Mutex
}

func WrapReadWriteCloser(r io.Reader, w io.Writer, closer func() error) io.ReadWriteCloser {
	return &ReadWriteCloser{
		r:      r,
		w:      w,
		closer: closer,
		closed: false,
	}
}

func (rwc *ReadWriteCloser) Read(p []byte) (n int, err error) {
	return rwc.r.Read(p)
}

func (rwc *ReadWriteCloser) Write(p []byte) (n int, err error) {
	return rwc.w.Write(p)
}

func (rwc *ReadWriteCloser) Close() error {
	rwc.lock.Lock()
	if rwc.closed {
		rwc.lock.Unlock()
		return nil
	}
	rwc.closed = true
	rwc.lock.Unlock()

	var err error
	if rc, ok := rwc.r.(io.Closer); ok {
		if err = rc.Close(); err != nil {
			return err
		}
	}

	if wc, ok := rwc.w.(io.Closer); ok {
		if err = wc.Close(); err != nil {
			return err
		}
	}

	if rwc.closer != nil {
		if err = rwc.closer(); err != nil {
			return err
		}
	}
	return nil
}

// Join two io.ReadWriteCloser and do some operations.
func Join(a io.ReadWriteCloser, b io.ReadWriteCloser) (toBCount int64, toACount int64) {
	var wait sync.WaitGroup
	pipe := func(from io.ReadWriteCloser, to io.ReadWriteCloser, count *int64) {
		defer to.Close()
		defer from.Close()
		defer wait.Done()

		buf := GetBuf(16 * 1024)
		defer PutBuf(buf)
		//从From读到To里
		*count, _ = io.CopyBuffer(to, from, buf)
	}

	wait.Add(2)
	go pipe(a, b, &toBCount)
	go pipe(b, a, &toACount)
	wait.Wait()
	return
}

func Compress(rwc io.ReadWriteCloser) io.ReadWriteCloser {
	sr := GetSnappyReader(rwc)
	sw := GetSnappyWriter(rwc)
	return WrapReadWriteCloser(sr, sw, func() error {
		err := rwc.Close()
		PutSnappyReader(sr)
		PutSnappyWriter(sw)
		return err
	})
}
