package ezUUID

import (
	"github.com/google/uuid"
	"github.com/youpy/go-base57"
)

func NewString() string {

	googleUuid, err := uuid.NewUUID()
	if err != nil {
		googleUuid = uuid.New()
	}

	return googleUuid.String()
}

func NewShortString() string {

	googleUuid, err := uuid.NewUUID()
	if err != nil {
		googleUuid = uuid.New()
	}
	encoder := base57.New()

	encodUuid := encoder.Encode(googleUuid)
	return encodUuid
}
