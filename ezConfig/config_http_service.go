package ezConfig

const DefaultForwarderKey = "X-Forwarder-for"
const DefaultHttpPort = 80
const DefaultHttpsPort = 443

type HttpServiceConfig struct {
	DebugMode bool                `yaml:"debug_mode"`
	Endpoint  HttpServiceEndpoint `yaml:"endpoint"`
	Http      HttpServicePlain    `yaml:"http"`
	Https     HttpServiceSecure   `yaml:"https"`
	Xff       HttpServiceXff      `yaml:"xff"`
	Cors      HttpServiceCors     `yaml:"cors"`
	Static    HttpServiceStatic   `yaml:"static"`
}

type HttpServiceEndpoint struct {
	BaseUri  string `yaml:"base_uri"`
	Internet string `yaml:"internet"`
	Intranet string `yaml:"intranet"`
	Prefix   string `yaml:"prefix"`
}

type HttpServiceCors struct {
	AllowHeaders     []string `yaml:"allow_headers"`
	AllowMethods     []string `yaml:"allow_methods"`
	AllowOrigin      []string `yaml:"allow_origin"`
	ExposeHeaders    []string `yaml:"expose_headers"`
	AllowCredentials bool     `yaml:"allow_credentials"`
	MaxAge           int      `yaml:"max_age"`
}

type HttpServicePlain struct {
	Enable bool   `yaml:"enable"`
	Host   string `yaml:"host"`
	Port   int    `yaml:"port"`
}

type HttpServiceSecure struct {
	Enable      bool   `yaml:"enable"`
	Host        string `yaml:"host"`
	Port        int    `yaml:"port"`
	TrustCaFile string `yaml:"trust_ca_file"` //CA根证书
	CertFile    string `yaml:"cert_file"`     //私钥证书
	CertPass    string `yaml:"cert_pass"`     //私钥密码
	KeyFile     string `yaml:"key_file"`      //公钥证书
}

type HttpServiceXff struct {
	Enable  bool   `yaml:"enable"`
	Keyword string `yaml:"keyword"`
	Index   int    `yaml:"index"`
}

type HttpServiceStatic struct {
	StaticRoot string `yaml:"static_root"`
	LayoutRoot string `yaml:"layout_root"`
}
