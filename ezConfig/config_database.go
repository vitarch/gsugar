package ezConfig

type DBConfig struct {
	Driver    string `yaml:"driver"`
	DBConnect string `yaml:"DBConnect"`
}

type DBParam struct {
	MaxConn     int    `yaml:"max_conn"`
	MaxIdle     int    `yaml:"max_idle"`
	MaxLifetime string `yaml:"conn_max_lifetime"` //such as 1m 30s
	MaxIdletime string `yaml:"conn_max_idletime"` //such as 1m 30s
}
