package ezConfig

type LogConfig struct {
	Level  string    `yaml:"level"`  //日志级别,debug,info,warn
	Path   string    `yaml:"path"`   //日志路径
	Rotate LogRotate `yaml:"rotate"` //日志分割时长,单位是小时,默认是1小时切一个文件
}

type LogRotate struct {
	MaxSize    int  `yaml:"max_size"`    //日志文件最大尺寸(MB)
	MaxAge     int  `yaml:"max_age"`     //日志最大保留天数,0为保留所有
	MaxBackups int  `yaml:"max_backups"` //日志文件保留份数,0为保留所有
	Compress   bool `yaml:"compress"`    //日志备份文件是否压缩存储
}
