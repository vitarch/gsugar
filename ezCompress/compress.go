package ezCompress

import (
	"bytes"
	"compress/gzip"
	"io"
)

// gzip压缩字符串
func GzCompress(src []byte) (dst []byte, err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	if _, err = gz.Write(src); err != nil {
		return
	}
	if err = gz.Flush(); err != nil {
		return
	}
	if err = gz.Close(); err != nil {
		return
	}
	dst = b.Bytes()
	return
}

// gzip解压缩字符串
func GzUnCompress(src []byte) (dst []byte, err error) {

	rdata := bytes.NewReader(src)
	r, err := gzip.NewReader(rdata)
	if err != nil {
		return
	}
	s, err := io.ReadAll(r)
	if err != nil {
		return
	}
	dst = s
	return dst, nil
}
